<?php
/*
Autor   : Luca.BASSI
Date    : 16.12.2019
*/

require 'controler/controler.php';

if (isset($_GET['action'])) {
    switch ($_GET['action']) {
        case 'details' :
            details();
            break;

        case 'login' :
            login();
            break;

        case 'logout' :
            logout();
            break;

        case 'goCheckLogin' :
            goCheckLogin();
            break;

        case 'creatUser' :
            creatUser();
            break;

        case 'goCreatAccount' :
            goCreatAccount();
            break;


        case 'goMagasin' :
            goMagasin();
            break;

        case 'goCommand' :
            goCommand();
            break;

        case 'goPanier' :
            goPanier();
            break;


        case 'goGestion' :
            goGestion();
            break;


        case 'addPanier' :
            addPanier();
            break;

        case 'delPanier' :
            delPanier();
            break;

        case 'userDelSnow' :
            userDelSnow();
            break;

        case 'gestion' :
            gestion();
            break;

        case 'createUserAdmin' :
            createUserAdmin();
            break;

        case 'addASnow' :
            addASnow();
            break;

        case 'newSnow' :
            newSnow();
            break;

        case 'delSnow' :
            delSnow();
            break;

        default :
            home();
            break;
    }
} else {
    home();
}
