<?php
$json = json_decode('{"S01":["001.cbf","002.cbf","003.cbf","004.cbf","005.cbf","006.cbf","007.cbf","008.cbf","009.cbf"],"S02":["001.sda","002.sda","003.sda"],"S03":["001.klm","002.klm"]}');
foreach($json as $key => $val) {
    echo "KEY IS: $key<br/>";
    foreach(((array)$json)[$key] as $val2) {
        echo "VALUE IS: $val2<br/>";
    }
}
?>
<?php
$response = json_decode('
{
  "code": 26,
  "result": {
    "0": {
      "cpu": {
        "423": {
          "prod": "Intel",
          "model": "i5-8300H"
        },
        "424": {
          "prod": "Intel",
          "model": "i7-8750H"
        }
      }
    }
  }
}');
$results = $response->result;
foreach ($results as $item) {
    foreach ($item->cpu as $key => $cpu) {
        echo "$key\n";
    }
}?>
<?php

$array = array(2 => 'a', 1 => 'b', 0 => 'c');

array_unshift($array, false); // Add to the start of the array
$array = array_values($array); // Re-number

// Remove the first index so we start at 1
$array = array_slice($array, 1, count($array), true);

print_r($array); // Array ( [1] => a [2] => b [3] => c )


$arr = array(
'2' => 'red',
'1' => 'green',
'0' => 'blue',
);

$arr1 = array_values($arr);   // Reindex the array starting from 0.
array_unshift($arr1, null);     // Prepend a dummy element to the start of the array.
//unset($arr1[0]);              // Kill the dummy element.

print_r($arr);
print_r($arr1);
?>